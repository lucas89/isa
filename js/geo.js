/**
 * Created by Lukasz on 2016-05-05.
 */
$(document).ready(function () {
        var row = "<tr><td class='t'></td>" +
            "<td class='xReal'></td>" +
            "<td class='xBin'></td>" +
            "<td class='fxReal'></td>" +
            "<td class='xRealBest'></td>" +
            "<td class='fxBest'></td></tr>";

        function addRows(a, b, tau, t, d, dValue) {
            var xRealAfterMutation = 0;
            var fxAfterMutation = [];
            var xBinAfterMutation;
            var fxBest = 0;
            var xBins = [];
            var xReals = [];
            var fxReals = [];
            var ranks = [];
            var probability = [];
            var fxMax = [];
            var xMax = [];
            var resultX = -9999;
            var resultY = -9999;

            for (var k = 0; k < t; k++) {
                $("#resultTable tbody").append(row);
            }
            var xReal = generateRandom(a, b, d);
            var l = calculateL(a, b, dValue);
            var xBin;
            var fxReal;
            for (var m = 0; m < t; m++) {
                setRowValue(m, 't', m + 1);
                setRowValue(m, 'xReal', xReal);
                xBin = realToBin(a, b, xReal, l);
                setRowValue(m, 'xBin', xBin);
                fxReal = calculateFx(xReal, d);
                setRowValue(m, 'fxReal', fxReal);
                for (var k = 0; k < l; k++) {
                    xReals[k] = binToReal(a, b, l, d, xBin.changeBit(k));
                    fxReals[k] = calculateFx(xReals[k], d);
                }
                ranks = makeRanking(fxReals, l);
                probability = makeProbability(ranks, tau, l);
				xBinAfterMutation = xBin;
                for (var k = 0; k < l; k++) {
                    if (isMutable(probability[k])) {
                        xBinAfterMutation = xBinAfterMutation.changeBit(k);
                        console.log("mutowany bit: " + k);
                    }
                }
                xRealAfterMutation = binToReal(a, b, l, d, xBinAfterMutation);
                fxAfterMutation[m] = calculateFx(xRealAfterMutation, d);

                if (m > 0) {
                    if (fxMax[m - 1] < fxAfterMutation[m]) {
                        fxMax[m] = fxAfterMutation[m];
                        xMax[m] = xRealAfterMutation;
                    } else {
                        fxMax[m] = fxMax[m - 1];
                        xMax[m] = xMax[m - 1];
                    }
                } else {
                    xMax[m] = xRealAfterMutation;
                    fxMax[m] = fxAfterMutation[m];
                }
                if (resultY < fxAfterMutation[m]) {
                    resultY = fxAfterMutation[m];
                    resultX = xMax[m];
                }
                setRowValue(m, 'xRealBest', xMax[m]);
                setRowValue(m, 'fxBest', fxMax[m]);
                xReal = xRealAfterMutation;
            }
            return {fx: fxAfterMutation, fxMax: fxMax, answerX: resultX, answerY: resultY};
        }


        /** Dodatkowe funkcje **/
        function resetCanvas() {
            $('#myChart').remove();
            $('#graphContainer').append('<canvas id="myChart" height="50"></canvas>');
        }

        function drawChart(generationsSize, fx, fxMaxes) {
            var generationsList = [];
            for (var k = 0; k < generationsSize; k++) {
                generationsList[k] = "T" + (k + 1);
            }
            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: generationsList,
                    datasets: [
                        {
                            label: 'f(x)',
                            data: fx,
                            borderWidth: 2,
                            borderColor: "green",
                            fill: false
                        },
                        {
                            label: 'MAX f(x)',
                            data: fxMaxes,
                            borderWidth: 3,
                            borderColor: "red",
                            fill: false
                        }
                    ]
                }
            });

        }

        function isMutable(pm) {
            var r = generateRandom(0, 1, 3);
            if (r <= pm) {
                return true;
            }
            return false;
        }

        function makeProbability(ranks, tau, l) {
            var probability = [];
            for (var k = 0; k < l; k++) {
                probability[k] = 1 / (Math.pow(ranks[k].bit, tau));
            }
            return probability;
        }

        function makeRanking(fxArray, l) {
            var ranks = [];
            for (var k = 0; k < l; k++) {
                ranks[k] = [];
                ranks[k].bit = k + 1;
                ranks[k].fx = fxArray[k];
            }
            var sort_by = function (field, reverse, primer) {
                var key = primer ?
                    function (x) {
                        return primer(x[field])
                    } :
                    function (x) {
                        return x[field]
                    };
                reverse = !reverse ? 1 : -1;
                return function (a, b) {
                    return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
                }
            };
            ranks.sort(sort_by('fx', true, parseFloat));
            return ranks;
        }

        function binToReal(a, b, l, d, xbin) {
            var intNumber = binToInt(xbin);
            return intToReal(a, b, intNumber, l, d);
        }

        function intToReal(a, b, xi2, l, d) {
            return ((( (b - a) * xi2) / (Math.pow(2, l) - 1)) + a).toFixed(d);
        }

        function realToBin(a, b, real, l) {
            var xInt = realToInt(a, b, real, l);
            return intToBin(xInt, l);
        }

        function binToInt(xi1) {
            return parseInt(xi1, 2);
        }

        String.prototype.changeBit = function (index) {
            var chr;
            if (this.substr(index,1) == 0) {
                chr = 1;
            } else {
                chr = 0;
            }
            if (index > this.length - 1) return this;
            return this.substr(0, index) + chr + this.substr(index + 1);
        };

        function calculateFx(xr2, d) {
            var func = (xr2 % 1) * ( Math.cos((20 * Math.PI * xr2)) - Math.sin(xr2));
            return func.toFixed(d);
        }

        function setRowValue(rowNr, colId, newValue) {
            $("tbody tr:eq(" + rowNr + ")").find('td.' + colId).html(newValue);
        }

        function removeRows() {
            $("#resultTable").find("tr:gt(0)").remove();
            $("#resultTableInPercents").empty();
        }

        function generateRandom(a, b, d) {
            var randomNumber = (Math.random() * (b - a) + a);
            return randomNumber.toFixed(d);
        }

        function realToInt(a, b, xr1, l) {
            return Math.round((1 / (b - a)) * (xr1 - a) * (Math.pow(2, l) - 1));
        }

        function intToBin(xi1, l) {
            var n = xi1.toString(2);
            var digits = Array(l + 1).join("0");
            n = digits.substr(n.length) + n;
            return n;
        }

        function calculateL(a, b, d) {
            return Math.ceil(Math.log(((b - a) / d) + 1) / Math.log(2));
        }

        function showAnswer(answerX, answerY) {
            $("#resultTableInPercents").append('Best X = ' + answerX + '  Best fx = ' + answerY).show();
        }

        $('#count').click(function () {
            resetCanvas();
            var newTableObject = document.getElementById("resultTable");
            sorttable.makeSortable(newTableObject);
            var dValue = parseFloat($("#d option:selected").text());
            var d = parseInt($("#d option:selected").val());
            var a = parseInt($('#a').val());
            var b = parseInt($('#b').val());
            var tau = parseInt($('#tau').val());
            var t = parseInt($('#t').val());
            removeRows();
            results = addRows(a, b, tau, t, d, dValue);
            drawChart(t, results.fx, results.fxMax);
            showAnswer(results.answerX, results.answerY);
        });

        $('#reset').click(function () {
            resetCanvas();
            removeRows();
            $("#resultTableInPercents").hide();
        });

    }
)
;