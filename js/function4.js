/**
 * Created by Lukasz on 2016-05-05.
 */
$(document).ready(function () {
        var row = "<tr><td class='nr'></td>" +
            "<td class='xr2'></td>" +
            "<td class='fx'></td>" +
            "<td class='gx'></td>" +
            "<td class='px'></td>" +
            "<td class='qx'></td>" +
            "<td class='r'</td>" +                // liczba r
            "<td class='xrS'></td>" +            // X Real po Selekcji
            "<td class='xbS'></td>" +            // X Bin po Selekcji
            "<td class='xbR'></td>" +        // X Bin Rodzica
            "<td class='pc'>0</td>" +          // punkt cięcia
            "<td class='xbP'></td>" +  //X Bin Potomstwa
            "<td class='xbK'></td>" +  //X Bin Po Krzyżowaniu
            "<td class='bins'></td>" +  // Biny krzyżowane
            "<td class='xbinM'></td>" +  // Biny po mutacji
            "<td class='xReal'></td>" +  // X real po Mutacji
            "<td class='fx2'></td></tr>";  // f(x)
        var gen;

        function addRows(a, b, n, dValue, d, pk, pm, T, xRealPrevious) {
            var fx1Array = [];
            var gxArray = [];
            var pxArray = [];
            var qxArray = [];
            var xrArray = [];
            var xbRArray = [];
            var xbsArray = [];
            var xbkArray = [];
            var visited = [];
            var xReal = [];
            var fx2Array = [];

            gen = "generation" + T;

            $("#resultTable").append('<tbody id="generation' + T + '"></tbody>');

            for (var k = 0; k < n; k++) {
                $("#" + gen).append(row);
            }
            for (var k = 0; k < n; k++) {
                var l = calculateL(a, b, dValue);
                var xr1;
                if (T === 0) {
                    xr1 = generateRandom(a, b, d);
                } else {
                    xr1 = xRealPrevious[k];
                }

                var xi1 = realToInt(a, b, xr1, l);
                var xb1 = intToBin(xi1, l);
                var xi2 = binToInt(xb1);
                xrArray[k] = intToReal(a, b, xi2, l);
                fx1Array[k] = calculateFx(xrArray[k]);
                setRowValue(k, 'nr', k + 1 + " T(" + T + ")");

                setRowValue(k, 'xr2', xrArray[k]);
                setRowValue(k, 'fx', fx1Array[k]);

                var fMin = findMin(fx1Array);
                var gx = fx1Array[k] - fMin + dValue;
                gxArray[k] = gx;
                setRowValue(k, 'gx', gx.toFixed(d + 1));
            }

            var gxSum = sumArrayElements(gxArray);
            var px, qx;
            for (var k = 0; k < n; k++) {
                px = gxArray[k] / gxSum;
                pxArray[k] = px;
                setRowValue(k, 'px', px.toFixed(d + 1));
                if (k == 0) {
                    qx = pxArray[0];
                } else {
                    qx = pxArray[k] + qxArray[k - 1]
                }
                qxArray[k] = qx;
                setRowValue(k, 'qx', qx.toFixed(d + 1));
            }

            for (var k = 0; k < n; k++) {
                var xrS = 0;
                var xbS = 0;
                var xbR = 0;
                var pc = 0;
                var r = generateRandom(0, 1, 12);
                var xbP = 0;
                setRowValue(k, 'r', r);
                for (var i = 0; i < n; i++) {
                    if (r < qxArray[0] && !visited[i]) {
                        xrS = xrArray[i];
                        visited[i] = true;
                        break;
                    }
                    if (r > qxArray[i] && r < qxArray[i + 1]) {
                        xrS = xrArray[i + 1];
                        break;
                    }
                }
                setRowValue(k, 'xrS', xrS);

                xbS = intToBin(realToInt(a, b, xrS, l), l);
                xbsArray[k] = xbS;
                setRowValue(k, 'xbS', xbS);
                if (r < pk) {
                    xbR = xbS;
                    xbRArray[k] = xbR;
                } else {
                    xbR = "-----";
                    xbRArray[k] = 0;
                }
                visited[k] = false;
                setRowValue(k, 'xbR', xbR);

            }

            /** wyznaczany Punkt Cięcia  **/
            var X = 0;
            var Y = 0;
            visited = [];
            for (var i = 0; i < n; i++) {
                X = 0;
                Y = 0;
                if (xbRArray[i] == 0 && !visited[i]) {
                    setRowValue(i, 'xbP', '-----');
                    setRowValue(i, 'xbK', xbsArray[i]);
                    xbkArray[i] = xbsArray[i];
                    visited[i] = true;
                } else if (!visited[i]) {
                    /*Liczony X*/
                    X = xbRArray[i];
                    pc = generateRandom(1, l - 1, 0);
                    setRowValue(i, 'pc', pc);
                    var temp1 = X.substring(0, pc);
                    var temp11 = X.substring(pc, X.length);
                    visited[i] = true;
                    for (var j = i + 1; j < n; j++) {
                        if (xbRArray[j] != 0 && !visited[j]) {
                            /*Liczony Y*/
                            Y = xbRArray[j];
                            setRowValue(j, 'pc', pc);
                            setRowValue(j, 'xbK', Y);
                            var temp2 = Y.substring(0, pc);
                            var temp22 = Y.substring(pc, Y.length);
                            visited[j] = true;
                            if (X != 0) {
                                setRowValue(i, 'xbP', temp2 + temp11);
                                setRowValue(i, 'xbK', temp2 + temp11);
                                xbkArray[i] = temp2 + temp11;
                            }
                            if (Y != 0) {
                                setRowValue(j, 'xbP', temp1 + temp22);
                                setRowValue(j, 'xbK', temp1 + temp22);
                                xbkArray[j] = temp1 + temp22;
                            }
                            break;
                        }
                    }
                    if (Y == 0) {
                        setRowValue(i, 'xbP', xbsArray[i]);
                        setRowValue(i, 'xbK', xbsArray[i]);
                        xbkArray[i] = xbsArray[i];
                    }
                }
                /** Zmutowane bity oraz biny po mutacji **/
                for (var k = 0; k < l; k++) {
                    var bin = xbkArray[i].substring(k, k + 1);
                    if (isMutable(pm)) {
                        appendToRowValue(i, 'bins', (k + 1) + " ");
                        if (bin == 0) {
                            appendToRowValue(i, 'xbinM', 1);
                        } else {
                            appendToRowValue(i, 'xbinM', 0);
                        }
                    } else {
                        appendToRowValue(i, 'xbinM', bin);
                    }
                }
                var tempInt = binToInt(getValueFromRow(i, 'xbinM'));
                xReal[i] = intToReal(a, b, tempInt, l);
                fx2Array[i] = calculateFx(xReal[i]);
                setRowValue(i, 'xReal', xReal[i]);
                setRowValue(i, 'fx2', fx2Array[i]);

            }

            return {max: findMax(fx2Array), min: findMin(fx2Array), avg: countAvg(fx2Array), xReal: xReal, fxReal: fx2Array};

        }


        /* ============ Funkcje pomocnicze ==================*/

        function isMutable(pm) {
            var r = generateRandom(0, 1, 3);
            if (r <= pm) {
                return true;
            }
            return false;
        }

        function sumArrayElements(array) {
            var sum = 0;
            for (var k = 0; k < array.length; k++) {
                sum = sum + parseFloat(array[k]);
            }
            return sum;
        }

        function removeRows() {
            $("#resultTable").find("tr:gt(0)").remove();
            $("#resultTableInPercents").find("tr:gt(0)").remove();
        }

        function findMin(array) {
            return Math.min.apply(Math, array);
        }

        function findMax(array) {
            return Math.max.apply(Math, array);
        }

        function countAvg(array) {
            return (sumArrayElements(array)) / array.length;
        }


        function generateRandom(a, b, d) {
            var randomNumber = (Math.random() * (b - a) + a);
            return randomNumber.toFixed(d);
        }

        function realToInt(a, b, xr1, l) {
            return Math.round((1 / (b - a)) * (xr1 - a) * (Math.pow(2, l) - 1));
        }

        function intToReal(a, b, xi2, l) {
            d = $("#d option:selected").val();
            return ((( (b - a) * xi2) / (Math.pow(2, l) - 1)) + a).toFixed(d);
        }

        function intToBin(xi1, l) {
            var n = xi1.toString(2);
            var digits = Array(l + 1).join("0");
            n = digits.substr(n.length) + n;
            return n;
        }

        function calculateFx(xr2) {
            d = $("#d option:selected").val();
            var func = (xr2 % 1) * ( Math.cos((20 * Math.PI * xr2)) - Math.sin(xr2));
            return func.toFixed(d);
        }

        function binToInt(xi1) {
            return parseInt(xi1, 2);
        }

        function calculateL(a, b, d) {
            return Math.ceil(Math.log(((b - a) / d) + 1) / Math.log(2));
        }


        function setRowValue(rowNr, colId, newValue) {
            $("#" + gen + " tr:eq(" + rowNr + ")").find('td.' + colId).html(newValue);
        }

        function appendToRowValue(rowNr, colId, newValue) {
            $("#" + gen + " tr:eq(" + rowNr + ")").find('td.' + colId).append(newValue);
        }

        function getValueFromRow(rowNr, colId) {
            return $("#" + gen + " tr:eq(" + rowNr + ")").find('td.' + colId).html();
        }

        Number.prototype.toRadians = function () {
            return this * Math.PI / 180;
        };

        function drawChart(generationsSize, allMax, allMin, allAvg) {
            var generationsList = [];
            for (var k = 0; k < generationsSize; k++) {
                generationsList[k] = "T" + (k + 1);
            }
            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: generationsList,
                    datasets: [
                        {
                            label: 'Max f(x)',
                            data: allMax,
                            borderWidth: 2,
                            borderColor: "red",
                            fill: false
                        },
                        {
                            label: 'Min f(x)',
                            data: allMin,
                            borderWidth: 2,
                            borderColor: "green",
                            fill: false
                        },
                        {
                            label: 'Average f(x)',
                            data: allAvg,
                            borderWidth: 2,
                            borderColor: "yellow",
                            fill: false
                        }
                    ]
                }
            });

        }

        function summaryResult(xReal, fxReal) {
            var summary = [];
            summary.xReal = [];
            summary.fxReal = [];
            summary.count = [];
            summary.perc = [];

            for (var i = 0; i < xReal.length; i++) {

                var contains = false;
                var index = -1;
                for (var j = 0; j < summary.xReal.length; j++) {
                    if(xReal[i] === summary.xReal[j]) {
                        contains = true;
                        index = j;
                    }
                }

                if(!contains) {
                    console.log("nie zawiera");
                    summary.xReal.push(xReal[i]);
                    summary.fxReal.push(fxReal[i]);
                    summary.count.push(1);
                } else {
                    console.log("zawiera");
                    summary.count[index] = summary.count[index] + 1;
                }
            }

            var sumCount = sumArrayElements(summary.count);
            for (var i = 0; i < summary.count.length; i++) {
                summary.perc[i] = (summary.count[i] / sumCount * 100).toFixed(2);
            }

            return summary;

        }

        function resultsToTable(xArray,fxArray,percentsArray) {
            for (var i = 0; i < xArray.length; i++) {
                $("#resultTableInPercents tbody").append('<tr>' +
                    '<td>'+(i+1)+'</td>' +
                    '<td>'+xArray[i]+'</td>' +
                    '<td>'+fxArray[i]+'</td>' +
                    '<td>'+percentsArray[i]+'</td></tr>');
            }
            $('#resultTableInPercents').show();
        }

        $('#count').click(function () {
            var dValue = parseFloat($("#d option:selected").text());
            var d = parseInt($("#d option:selected").val());
            var a = parseInt($('#a').val());
            var b = parseInt($('#b').val());
            var T = parseInt($('#t').val());
            var N = parseInt($('#n').val());
            var pk = parseFloat($('#pk').val());
            var pm = parseFloat($('#pm').val());
            var results;
            removeRows();
            var allMax = [];
            var allMin = [];
            var allAvg = [];
            var Xreal = [];
            for (var i = 0; i < T; i++) {
                results = addRows(a, b, N, dValue, d, pk, pm, i, Xreal);
                allMax.push(results.max);
                allMin.push(results.min);
                allAvg.push(results.avg);
                Xreal = results.xReal;
            }

            console.log(allMax);
            console.log(allMin);
            console.log(allAvg);
            drawChart(T, allMax, allMin, allAvg);

            var summary = summaryResult(results.xReal, results.fxReal);
            console.log("summary" + summary.xReal);
            console.log("summary" + summary.fxReal);
            console.log("summary" + summary.count);
            console.log("summary" + summary.perc);

            resultsToTable(summary.xReal, summary.fxReal, summary.perc);

            var newTableObject = document.getElementById("resultTableInPercents");
            sorttable.makeSortable(newTableObject);

            var myTH = newTableObject.getElementsByTagName("th")[3];
            sorttable.innerSortFunction.apply(myTH, []);
            sorttable.innerSortFunction.apply(myTH, []);

            console.log("Srednia średnich: " + countAvg(allAvg));

        });

    }
)
;