/**
 * Created by Lukasz on 2016-05-05.
 */
$(document).ready(function () {
        var row = "<tr><td class='t'></td>" +
            "<td class='xReal1'></td>" +
            "<td class='xReal2'></td>" +
            "<td class='xBin'></td>" +
            "<td class='fxReal'></td>" +
            "<td class='xRealBest1'></td>" +
            "<td class='xRealBest2'></td>" +
            "<td class='fxBest'></td></tr>";

        function addRows(a, b, tau, t, d, dValue) {
            var xRealAfterMutation1 = 0;
            var xRealAfterMutation2 = 0;
            var fxAfterMutation = [];
            var xBinAfterMutation;
            var xBinAfterMutation1;
            var xBinAfterMutation2;
            var fxBest = 0;
            var xReals1 = [];
            var xReals2 = [];
            var fxReals = [];
            var ranks = [];
            var probability = [];
            var fxMax = [];
            var xMax1 = [];
            var xMax2 = [];
            var resultX = 9999;
            var resultX2 = 9999;
            var resultY = 9999;

            for (var k = 0; k < t; k++) {
                $("#resultTable tbody").append(row);
            }
            var xReal1 = generateRandom(a, b, d);
            var xReal2 = generateRandom(a, b, d);
            var l = calculateL(a, b, dValue);
            var xBin;
            var x1Bin;
            var x2Bin;
            var fxReal;
            for (var m = 0; m < t; m++) {
                xBin = null;
                setRowValue(m, 't', m + 1);
                setRowValue(m, 'xReal1', xReal1);
                setRowValue(m, 'xReal2', xReal2);
                x1Bin = realToBin(a, b, xReal1, l);
                x2Bin = realToBin(a, b, xReal2, l);
                xBin = x1Bin + x2Bin;
                setRowValue(m, 'xBin', xBin);
                fxReal = calculateFx(xReal1, xReal2, d);
                setRowValue(m, 'fxReal', fxReal);
                for (var k = 0; k < l * 2; k++) {
                    xReals1[k] = binToReal(a, b, l, d, x1Bin.changeBit(k));
                    xReals2[k] = binToReal(a, b, l, d, x2Bin.changeBit(k));
                    fxReals[k] = calculateFx(xReals1[k], xReals2[k], d);
                }
                ranks = makeRanking(fxReals, l * 2);
                probability = makeProbability(ranks, tau, l * 2);
                xBinAfterMutation = x1Bin + x2Bin;
                for (var k = 0; k < l * 2; k++) {
                    if (isMutable(probability[k])) {
                        xBinAfterMutation = xBinAfterMutation.changeBit(k);
                    }
                }
                var binLenght = xBinAfterMutation.length;
                xBinAfterMutation1 = xBinAfterMutation.substr(0, binLenght / 2);
                xBinAfterMutation2 = xBinAfterMutation.substr(binLenght / 2, binLenght);
                xRealAfterMutation1 = binToReal(a, b, l, d, xBinAfterMutation1);
                xRealAfterMutation2 = binToReal(a, b, l, d, xBinAfterMutation2);
                fxAfterMutation[m] = calculateFx(xRealAfterMutation1, xRealAfterMutation2, d);

                if (m > 0) {
                    if (fxMax[m - 1] > fxAfterMutation[m]) {
                        fxMax[m] = fxAfterMutation[m];
                        xMax1[m] = xRealAfterMutation1;
                        xMax2[m] = xRealAfterMutation2;
                    } else {
                        fxMax[m] = fxMax[m - 1];
                        xMax1[m] = xMax1[m - 1];
                        xMax2[m] = xMax2[m - 1];
                    }
                } else {
                    xMax1[m] = xRealAfterMutation1;
                    xMax2[m] = xRealAfterMutation2;
                    fxMax[m] = fxAfterMutation[m];
                }
                if (resultY > fxAfterMutation[m]) {
                    resultY = fxAfterMutation[m];
                    resultX = xMax1[m];
                    resultX2 = xMax2[m];
                }
                setRowValue(m, 'xRealBest1', xMax1[m]);
                setRowValue(m, 'xRealBest2', xMax2[m]);
                setRowValue(m, 'fxBest', fxMax[m]);
                xReal1 = xRealAfterMutation1;
                xReal2 = xRealAfterMutation2;
            }
            return {fx: fxAfterMutation, fxMax: fxMax, answerX1: resultX, answerX2: resultX2, answerY: resultY};
        }


        /** Dodatkowe funkcje **/
        function sumArrayElements(array) {
            var sum = 0;
            for (var k = 0; k < array.length; k++) {
                sum = sum + parseFloat(array[k]);
            }
            return sum;
        }

        function resetCanvas() {
            $('#myChart').remove();
            $('#graphContainer').append('<canvas id="myChart" height="50"></canvas>');
        }

        function drawChart(generationsSize, fx, fxMaxes) {
            var generationsList = [];
            for (var k = 0; k < generationsSize; k++) {
                generationsList[k] = "T" + (k + 1);
            }
            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: generationsList,
                    datasets: [
                        {
                            label: 'f(x)',
                            data: fx,
                            borderWidth: 2,
                            borderColor: "green",
                            fill: false
                        },
                        {
                            label: 'MIN f(x)',
                            data: fxMaxes,
                            borderWidth: 3,
                            borderColor: "red",
                            fill: false
                        }
                    ]
                }
            });

        }

        function drawChart2(generationsSize, precissTau, fx) {
            var generationsList = [];
            var tau = 1;
            for (var k = 0; k < generationsSize; k++) {
                tau+=precissTau;
                generationsList[k] = "tau " + tau.toFixed(3);
            }
            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: generationsList,
                    datasets: [
                        {
                            label: 'f(x)',
                            data: fx,
                            borderWidth: 2,
                            borderColor: "green",
                            fill: false
                        }
                    ]
                }
            });

        }

        function isMutable(pm) {
            var r = generateRandom(0, 1, 3);
            if (r <= pm) {
                return true;
            }
            return false;
        }

        function makeProbability(ranks, tau, l) {
            var probability = [];
            for (var k = 0; k < l; k++) {
                probability[k] = 1 / (Math.pow(ranks[k].bit, tau));
            }
            return probability;
        }

        function makeRanking(fxArray, l) {
            var ranks = [];
            for (var k = 0; k < l; k++) {
                ranks[k] = [];
                ranks[k].bit = k + 1;
                ranks[k].fx = fxArray[k];
            }
            var sort_by = function (field, reverse, primer) {
                var key = primer ?
                    function (x) {
                        return primer(x[field])
                    } :
                    function (x) {
                        return x[field]
                    };
                reverse = !reverse ? 1 : -1;
                return function (a, b) {
                    return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
                }
            };
            ranks.sort(sort_by('fx', true, parseFloat));
            return ranks;
        }

        function binToReal(a, b, l, d, xbin) {
            var intNumber = binToInt(xbin);
            return intToReal(a, b, intNumber, l, d);
        }

        function intToReal(a, b, xi2, l, d) {
            return ((( (b - a) * xi2) / (Math.pow(2, l) - 1)) + a).toFixed(d);
        }

        function realToBin(a, b, real, l) {
            var xInt = realToInt(a, b, real, l);
            return intToBin(xInt, l);
        }

        function binToInt(xi1) {
            return parseInt(xi1, 2);
        }

        String.prototype.changeBit = function (index) {
            var chr;
            if (this.substr(index, 1) == 0) {
                chr = 1;
            } else {
                chr = 0;
            }
            if (index > this.length - 1) return this;
            return this.substr(0, index) + chr + this.substr(index + 1);
        };

        function calculateFx(x1, x2, d) {
            var func = 2 * (Math.pow(x1, 2)) - 1.05 * (Math.pow(x1, 4)) + (Math.pow(x1, 6)) / 6 + x1 * x2 + Math.pow(x2, 2);
            return func.toFixed(d);
        }

        function setRowValue(rowNr, colId, newValue) {
            $("tbody tr:eq(" + rowNr + ")").find('td.' + colId).html(newValue);
        }

        function removeRows() {
            $("#resultTable").find("tr:gt(0)").remove();
            $("#resultTableInPercents").empty();
        }

        function generateRandom(a, b, d) {
            var randomNumber = (Math.random() * (b - a) + a);
            return randomNumber.toFixed(d);
        }

        function realToInt(a, b, xr1, l) {
            return Math.round((1 / (b - a)) * (xr1 - a) * (Math.pow(2, l) - 1));
        }

        function intToBin(xi1, l) {
            var n = xi1.toString(2);
            var digits = Array(l + 1).join("0");
            n = digits.substr(n.length) + n;
            return n;
        }

        function calculateL(a, b, d) {
            return Math.ceil(Math.log(((b - a) / d) + 1) / Math.log(2));
        }

        function showAnswer(answerX, answerX2, answerY) {
            $("#resultTableInPercents").append('Best X1 = ' + answerX + ' X2 = ' + answerX2 + '  Best fx = ' + answerY).show();
        }

        $('#count').click(function () {
            resetCanvas();
            var newTableObject = document.getElementById("resultTable");
            sorttable.makeSortable(newTableObject);
            var dValue = parseFloat($("#d option:selected").text());
            var d = parseInt($("#d option:selected").val());
            var a = parseInt($('#a').val());
            var b = parseInt($('#b').val());
            var tau = parseInt($('#tau').val());
            var t = parseInt($('#t').val());
            removeRows();
            results = addRows(a, b, tau, t, d, dValue);
            drawChart(t, results.fx, results.fxMax);
            showAnswer(results.answerX, results.answerY);
        });

        $('#test').click(function () {
            var newTableObject = document.getElementById("resultTable");
            sorttable.makeSortable(newTableObject);
            var dValue = parseFloat($("#d option:selected").text());
            var d = parseInt($("#d option:selected").val());
            var a = parseInt($('#a').val());
            var b = parseInt($('#b').val());
            var tau;
            var t = 12;
            var sumFxMax = 0;
            var avgFx = [];
			var sumAvg = 0;
            var avgFromTau;
			
            var testsCount = 20;
			
			var precissTau = 0.1;
			var startTau = 1.0;
			var stopTau = 2.0;
			var testTauCount = (stopTau - startTau) / precissTau;
			console.log("liczba tau dla ilu będą testy: " + testTauCount);

			tau = startTau;
			for (var j = 0; j < testTauCount; j++) {
				tau += precissTau;
				console.log('tau: ' + tau);
				sumAvg = 0;
				for (var big = 0; big < testsCount; big++){
					console.log("test " + big);
					sumFxMax = 0;
					for (var i = 0; i < t; i++) {
						removeRows();
						results = addRows(a, b, tau, t, d, dValue);
						sumFxMax += parseFloat(results.fx[i]);
					}
					avgFromTau = sumFxMax / t;
					sumAvg += avgFromTau;
				}
				
				avgFx[j] = sumAvg / testsCount;
			}
			
			console.log(avgFx);
            drawChart2(testTauCount, precissTau, avgFx);

        });

        $('#reset').click(function () {
            resetCanvas();
            removeRows();
            $("#resultTableInPercents").hide();
        });

    }
)
;