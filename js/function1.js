/**
 * Created by Lukasz on 2016-04-07.
 */
$(document).ready(function () {
    var row = "<tr><td class='nr'>nr</td><td class='xr1'>xr1</td><td class='xi1'>xi1</td><td class='xb1'>xb1</td><td class='xi2'>xi2</td><td class='xr2'>xr2</td><td class='xf'>xf</td></tr>";

    function addRows(a, b, n, d) {
        for (var k = 0; k < n; k++) {
            $("#resultTable tbody").append(row);
            var l = calculateL(a, b, d);
            var xr1 = generateRandoms(a, b);
            var xi1 = realToInt(a, b, xr1, l);
            var xb1 = intToBin(xi1, l);
            var xi2 = binToInt(xb1);
            var xr2 = intToReal(a, b, xi2, l);
            var xf = calculateFx(xr2);
            setRowValue(k + 1, 'nr', k + 1);
            setRowValue(k + 1, 'xr1', xr1);
            setRowValue(k + 1, 'xi1', xi1);
            setRowValue(k + 1, 'xb1', xb1);
            setRowValue(k + 1, 'xi2', xi2);
            setRowValue(k + 1, 'xr2', xr2);
            setRowValue(k + 1, 'xf', xf);
        }
    }

    function removeRows() {
        $("#resultTable").find("tr:gt(0)").remove();
    }


    function generateRandoms(a, b) {
        d = $("#d option:selected").val();
        var randomNumber = (Math.random() * (b - a) + a);
        return randomNumber.toFixed(d);
    }

    function realToInt(a, b, xr1, l) {
        return Math.round((1 / (b - a)) * (xr1 - a) * (Math.pow(2, l) - 1));
    }

    function intToReal(a, b, xi2, l) {
        d = $("#d option:selected").val();
        return ((( (b - a) * xi2) / (Math.pow(2, l) - 1)) + a).toFixed(d);
    }

    function intToBin(xi1, l) {
        var n = xi1.toString(2);
        var digits = Array(l + 1).join("0");
        n = digits.substr(n.length) + n;
        return n;
    }

    function calculateFx(xr2) {
        d = $("#d option:selected").val();
        var func = (xr2 % 1) * ( Math.cos((20 * Math.PI * xr2).toRadians()) - Math.sin((1*xr2).toRadians()) );
        return func.toFixed(d);
    }

    function binToInt(xi1) {
        return parseInt(xi1, 2);
    }

    function calculateL(a, b, d) {
        return Math.ceil(Math.log(((b - a) / d) + 1) / Math.log(2));
    }

    function setRowValue(rowNr, colId, newValue) {
        $("tr:eq(" + rowNr + ")").find('td.' + colId).html(newValue);
    }

    Number.prototype.toRadians = function () {
        return this * Math.PI / 180;
    }

    $('#count').click(function () {
        var d = $("#d option:selected").text();
        var a = parseInt($('#a').val());
        var b = parseInt($('#b').val());
        var N = parseInt($('#n').val());
        removeRows();
        addRows(a, b, N, d);
    });

});