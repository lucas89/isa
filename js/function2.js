/**
 * Created by Lukasz on 2016-05-05.
 */
$(document).ready(function () {
        var row = "<tr><td class='nr'></td>" +
            "<td class='xr2'></td>" +
            "<td class='fx'></td>" +
            "<td class='gx'></td>" +
            "<td class='px'></td>" +
            "<td class='qx'></td>" +
            "<td class='r'</td>" +                // liczba r
            "<td class='xrS'></td>" +            // X Real po Selekcji
            "<td class='xbS'></td>" +            // X Bin po Selekcji
            "<td class='xbR'></td>" +        // X Bin Rodzica
            "<td class='pc'>0</td>" +          // punkt cięcia
            "<td class='xbP'></td>" +  //X Bin Potomstwa
            "<td class='xbK'></td>" +  //X Bin Po Krzyżowaniu
            "<td class='bins'></td>" +  // Biny krzyżowane
            "<td class='xbinM'></td>" +  // Biny po mutacji
            "<td class='xReal'></td>" +  // X real po Mutacji
            "<td class='fx2'></td></tr>";  // f(x)

        function addRows(a, b, n, dValue, d, pk, pm) {
            var fxArray = [];
            var gxArray = [];
            var pxArray = [];
            var qxArray = [];
            var xrArray = [];
            var xbRArray = [];
            var xbsArray = [];
            var xbkArray = [];
            var visited = [];
            var xReal = [];

            for (var k = 0; k < n; k++) {
                $("#resultTable tbody").append(row);
                var l = calculateL(a, b, dValue);
                var xr1 = generateRandom(a, b, d);
                var xi1 = realToInt(a, b, xr1, l);
                var xb1 = intToBin(xi1, l);
                var xi2 = binToInt(xb1);
                xrArray[k] = intToReal(a, b, xi2, l);
                fxArray[k] = calculateFx(xrArray[k]);
                setRowValue(k + 1, 'nr', k + 1);
                setRowValue(k + 1, 'xr2', xrArray[k]);
                setRowValue(k + 1, 'fx', fxArray[k]);
            }

            var fxMin = findMin(fxArray);

            for (var k = 0; k < n; k++) {
                var gx = fxArray[k] - fxMin + dValue;
                gxArray[k] = gx;
                setRowValue(k + 1, 'gx', gx.toFixed(d + 1));
            }

            var gxSum = sumArrayElements(gxArray);

            var px, qx;
            for (var k = 0; k < n; k++) {
                px = gxArray[k] / gxSum;
                pxArray[k] = px;
                setRowValue(k + 1, 'px', px.toFixed(d + 1));

                if (k == 0) {
                    qx = pxArray[0];
                } else {
                    qx = pxArray[k] + qxArray[k - 1]
                }
                qxArray[k] = qx;
                setRowValue(k + 1, 'qx', qx.toFixed(d + 1));
            }

            for (var k = 0; k < n; k++) {
                var xrS = 0;
                var xbS = 0;
                var xbR = 0;
                var pc = 0;
                var r = generateRandom(0, 1, 12);
                var xbP = 0;
                setRowValue(k + 1, 'r', r);
                for (var i = 0; i < n; i++) {
                    if (r < qxArray[0] && !visited[i]) {
                        xrS = xrArray[i];
                        visited[i] = true;
                        break;
                    }
                    if (r > qxArray[i] && r < qxArray[i + 1]) {
                        xrS = xrArray[i + 1];
                        break;
                    }
                }
                setRowValue(k + 1, 'xrS', xrS);

                xbS = intToBin(realToInt(a, b, xrS, l), l);
                xbsArray[k] = xbS;
                setRowValue(k + 1, 'xbS', xbS);
                if (r < pk) {
                    xbR = xbS;
                    xbRArray[k] = xbR;
                } else {
                    xbR = "-----";
                    xbRArray[k] = 0;
                }
                visited[k] = false;
                setRowValue(k + 1, 'xbR', xbR);

            }

            /** wyznaczany Punkt Cięcia  **/
            var X = 0;
            var Y = 0;
            visited = [];
            for (var i = 0; i < n; i++) {
                X = 0;
                Y = 0;
                if (xbRArray[i] == 0 && !visited[i]) {
                    setRowValue(i + 1, 'xbP', '-----');
                    setRowValue(i + 1, 'xbK', xbsArray[i]);
                    xbkArray[i] = xbsArray[i];
                    visited[i] = true;
                } else if (!visited[i]) {
                    /*Liczony X*/
                    X = xbRArray[i];
                    pc = generateRandom(1, l - 1, 0);
                    setRowValue(i + 1, 'pc', pc);
                    var temp1 = X.substring(0, pc);
                    var temp11 = X.substring(pc, X.length);
                    visited[i] = true;
                    for (var j = i + 1; j < n; j++) {
                        if (xbRArray[j] != 0 && !visited[j]) {
                            /*Liczony Y*/
                            Y = xbRArray[j];
                            setRowValue(j + 1, 'pc', pc);
                            setRowValue(j + 1, 'xbK', Y);
                            var temp2 = Y.substring(0, pc);
                            var temp22 = Y.substring(pc, Y.length);
                            visited[j] = true;
                            if (X != 0) {
                                setRowValue(i + 1, 'xbP', temp2 + temp11);
                                setRowValue(i + 1, 'xbK', temp2 + temp11);
                                xbkArray[i] = temp2 + temp11;
                            }
                            if (Y != 0) {
                                setRowValue(j + 1, 'xbP', temp1 + temp22);
                                setRowValue(j + 1, 'xbK', temp1 + temp22);
                                xbkArray[j] = temp1 + temp22;
                            }
                            break;
                        }
                    }
                    if (Y == 0) {
                        setRowValue(i + 1, 'xbP', xbsArray[i]);
                        setRowValue(i + 1, 'xbK', xbsArray[i]);
                        xbkArray[i] = xbsArray[i];
                    }
                }
                /** Zmutowane bity oraz biny po mutacji **/
                for (var k = 0; k < l; k++) {
                    var bin = xbkArray[i].substring(k, k + 1);
                    if (isMutable(pm)) {
                        appendToRowValue(i + 1, 'bins', (k+1)+" ");
                        if(bin == 0){
                            appendToRowValue(i + 1, 'xbinM', 1);
                        }else{
                            appendToRowValue(i + 1, 'xbinM', 0);
                        }
                    } else {
                        appendToRowValue(i + 1, 'xbinM', bin);
                    }
                }
                var tempInt = binToInt(getValueFromRow(i+1,'xbinM'));
                var tempReal = intToReal(a,b,tempInt,l);
                setRowValue(i + 1, 'xReal', tempReal);
                setRowValue(i + 1, 'fx2', calculateFx(tempReal));
            }
        }


        /* ============ Funkcje pomocnicze ==================*/

        function isMutable(pm) {
            var r = generateRandom(0, 1, 3);
            if (r <= pm) {
                return true;
            }
            return false;
        }

        function sumArrayElements(array) {
            var sum = 0;
            for (var k = 0; k < array.length; k++) {
                sum = sum + array[k];
            }
            return sum;
        }

        function removeRows() {
            $("#resultTable").find("tr:gt(0)").remove();
        }

        function findMin(array) {
            return Math.min.apply(Math, array);
        }


        function generateRandom(a, b, d) {
            var randomNumber = (Math.random() * (b - a) + a);
            return randomNumber.toFixed(d);
        }

        function realToInt(a, b, xr1, l) {
            return Math.round((1 / (b - a)) * (xr1 - a) * (Math.pow(2, l) - 1));
        }

        function intToReal(a, b, xi2, l) {
            d = $("#d option:selected").val();
            return ((( (b - a) * xi2) / (Math.pow(2, l) - 1)) + a).toFixed(d);
        }

        function intToBin(xi1, l) {
            var n = xi1.toString(2);
            var digits = Array(l + 1).join("0");
            n = digits.substr(n.length) + n;
            return n;
        }

        function calculateFx(xr2) {
            d = $("#d option:selected").val();
            var func = (xr2 % 1) * ( Math.cos((20 * Math.PI * xr2)) - Math.sin(xr2));
            return func.toFixed(d);
        }

        function binToInt(xi1) {
            return parseInt(xi1, 2);
        }

        function calculateL(a, b, d) {
            return Math.ceil(Math.log(((b - a) / d) + 1) / Math.log(2));
        }


        function setRowValue(rowNr, colId, newValue) {
            $("tr:eq(" + rowNr + ")").find('td.' + colId).html(newValue);
        }

        function appendToRowValue(rowNr, colId, newValue) {
            $("tr:eq(" + rowNr + ")").find('td.' + colId).append(newValue);
        }

        function getValueFromRow(rowNr, colId) {
            return $("tr:eq(" + rowNr + ")").find('td.' + colId).html();
        }

        Number.prototype.toRadians = function () {
            return this * Math.PI / 180;
        }

        $('#count').click(function () {
            var dValue = parseFloat($("#d option:selected").text());
            var d = parseInt($("#d option:selected").val());
            var a = parseInt($('#a').val());
            var b = parseInt($('#b').val());
            var N = parseInt($('#n').val());
            var pk = parseFloat($('#pk').val());
            var pm = parseFloat($('#pm').val());
            removeRows();
            addRows(a, b, N, dValue, d, pk, pm);
        });

    }
)
;